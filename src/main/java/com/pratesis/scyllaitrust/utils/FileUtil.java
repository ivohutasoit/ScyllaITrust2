package com.pratesis.scyllaitrust.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Raja Yudha P. S.
 */
public class FileUtil {
    /**
     *
     * @param filepath
     * @param data
     * @throws java.lang.Exception
     */
    public static void write(String filepath, String data) throws Exception {
        try {
            File file = new File(filepath);

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fileWritter = new FileWriter(file.getAbsoluteFile(), true);
            try (BufferedWriter bufferWritter = new BufferedWriter(fileWritter)) {
                bufferWritter.write(data);
            }

        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
