package com.pratesis.scyllaitrust.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility for connection to database
 * @author Raja Yudha P. S.
 */
public class DBManager {
    private static ResourceBundle bundle = ResourceBundle.getBundle("global");
    
    private Connection connection;

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @return the connection
     */
    public boolean isConnect() {
        try {
            return !connection.isClosed();
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    /**
     * 
     * @throws java.lang.Exception
     */
    public void connect() throws Exception{
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(
                    String.format(bundle.getString("db.connection.format"), ScyConfig.getDbHost()),
                    bundle.getString("db.username"),
                    bundle.getString("db.password"));
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    /**
     * 
     * @throws java.lang.Exception
     */
    public void disconnect() throws Exception {
        try {
            getConnection().close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
