package com.pratesis.scyllaitrust.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Checking the scyconfig.ini file in root application
 *
 * @author Raja Yudha P. S.
 */
public class ScyConfig {
    private static ScyConfig instance = null;
    private static ResourceBundle bundle = ResourceBundle.getBundle("global");
    
    private static BufferedReader reader = null;
    
    private static String path;
    private static List<String> data;

    
    static {
        path = System.getProperty("user.dir")
                + File.separator + bundle.getString("app.config.file");
        data = new ArrayList<String>();
    }

    /**
     * Checking file scyconfig.ini is exist or not
     *
     * @return false if file was not exist true if file was exist
     */
    public static boolean isExist() {
        return new File(path).exists();
    }
    
    
    public static String getDbHost() {
        String host = "";
        readData();
        
        if(data.size() > 0) {
            host = data.get(0);
        }
        return host;
    }
    
    public static String getApiHost() {
        String host = "";
        readData();
        
        if(data.size() > 0) {
            host = data.get(1);
        }
        return host;
    }
    
    /**
     * 
     * @return 
     */
    public static boolean isDontShowDialog() {
        readData();
        
        if(data.size() > 0) {
            if(data.size() == 3)
                return data.get(2).equals("N");
            else
                return true;
        }
        return true;
    }
    /**
     * 
     */
    private static void readData() {
        try {
            if (isExist()) {
                String current = null;
                reader = new BufferedReader(new FileReader(path));
                while((current = reader.readLine()) != null) {
                    data.add(current);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScyConfig.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ScyConfig.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(ScyConfig.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
