CREATE OR REPLACE VIEW fitrust
AS
   SELECT a.pcode, a.pekan, a.convunit3,
          NVL (a.stakhirgd, 0) + NVL (a.stakhirvan, 0) stock,
          NVL (b.rpp, 0) rpp, a.buyprice3, CAST ('ITR' AS VARCHAR2 (10)) DATA1,
          CAST (NULL AS VARCHAR2 (10)) DATA2, CAST (NULL AS VARCHAR2 (10)) DATA3,
          CAST (NULL AS VARCHAR2 (10)) DATA4, CAST (NULL AS VARCHAR2 (10)) DATA5
     FROM fklp3mst a,
          (SELECT pcode, pekan, NVL (rpp, 0) rpp
             FROM fklp3sls
            WHERE tipe = 'PC') b
    WHERE a.pcode = b.pcode(+) AND a.pekan = b.pekan(+);    



CREATE OR REPLACE VIEW fitrust1
AS
SELECT   a.tahun "Tahun", pekan "Pekan", b.kodedist "KdDist", pcode "PCode",
         TO_CHAR (ROUND (stock / convunit3, 4),
                  'FM999999999990.0000'
                 ) "St_crt",
         TO_CHAR (ROUND (rpp / convunit3, 4), 'FM999999999990.0000') "RR_crt",
         TO_CHAR (ROUND ((stock / convunit3) * buyprice3, 4),
                  'FM999999999990.0000'
                 ) "St_val",
         TO_CHAR (ROUND ((rpp / convunit3) * buyprice3, 4),
                  'FM999999999990.0000'
                 ) "RR_val",
         data1 "Data1", data2 "Data2", data3 "Data3", data4 "Data4",
         data5 "Data5"
    FROM fitrust,
         (SELECT DISTINCT TO_CHAR (cdate, 'YYYY') tahun
                     FROM fcycle3
                    WHERE prdno = 5) a,
         (SELECT memostring kodedist
            FROM fmemo
           WHERE memonama = 'KODEDIST') b
   WHERE pekan =
            (SELECT DECODE (SUM (pkn1), 0, SUM (pkn2), SUM (pkn1)) pkn
               FROM (SELECT NVL (memoint, 0) pkn1, 0 pkn2
                       FROM fmemo
                      WHERE memonama = 'PKN_ITRUST'
                     UNION ALL
                     SELECT 0 pkn1, memoint-1 pkn2
                       FROM fmemo
                      WHERE memonama = 'PEKAN'))
ORDER BY pcode

insert into fmemo (memonama,memostring) values ('REL_ITRUST','Y')
insert into fmemo (memonama,memostring,memostring2) values ('USR_ITRUST','superadmin@itrust.com','12345678')
create table FHIS_ITRUST  (tglgd date,snow varchar2(25),ket varchar2(25),Relno varchar2(5))
=============
1.Login Scylla
2.select memostring from fmemo where memonama='REL_ITRUST'
   jika 'Y' lanjut
   Jika 'T' ==>write ke C:\TEMP\DATASCY\ESITR_yyyymmddhhnnss.txt (Rel Scylla utk iTrust belum ada)
3.select memostring nmuser,memostring2 pwd from fmemo where memonama='USR_ITRUST'
  Jika tdk ok , write ke table FHIS_ITRUST
 	insert into FHIS_ITRUST(TGLGD, SNOW, KET,Relno) 
	SELECT A.TGLGD,B.sNow ,'GAGAL KONEKSI KE ITRUST' KET,'v1' relno FROM (
	select MEMODATE TGLGD from fmemo where memonama='CADATE'
	)A,(
	select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') sNow FROM DUAL)B

  Jika token ok , write ke table FHIS_ITRUST
 	insert into FHIS_ITRUST(TGLGD, SNOW, KET,Relno) 
	SELECT A.TGLGD,B.sNow ,'token ok' KET,'v1' relno FROM (
	select MEMODATE TGLGD from fmemo where memonama='CADATE'
	)A,(
	select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') sNow FROM DUAL)B

 Jika ok , write ke table FHIS_ITRUST
 	insert into FHIS_ITRUST(TGLGD, SNOW, KET,Relno) 
	SELECT A.TGLGD,B.sNow ,'START KONEKSI KE ITRUST' KET,'v1' relno FROM (
	select MEMODATE TGLGD from fmemo where memonama='CADATE'
	)A,(
	select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') sNow FROM DUAL)B

4.  CEK TBL FITRUST1
  JIKA TIDAK ADA DATA write ke table FHIS_ITRUST
	insert into FHIS_ITRUST(TGLGD, SNOW, KET,Relno) 
	SELECT A.TGLGD,B.sNow ,'TIDAK ADA DATA' KET,'v1' relno FROM (
	select MEMODATE TGLGD from fmemo where memonama='CADATE'
	)A,(
	select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') sNow FROM DUAL)B

   JIKA ADA DATA
SELECT A.TGLGD,B.sNow ,'SEND THN:'||C.AA KET,'v1' relno FROM (
select MEMODATE TGLGD from fmemo where memonama='CADATE'
)A,(
select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') sNow FROM DUAL)B
,(SELECT DISTINCT "Tahun"||'-'||"Pekan" AA FROM fitrust1)C

SELECT A.TGLGD,B.sNow ,'SELESAI SEND DATA THN:'||C.AA KET,'v1' relno FROM (
select MEMODATE TGLGD from fmemo where memonama='CADATE'
)A,(
select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') sNow FROM DUAL)B
,(SELECT DISTINCT "Tahun"||'-'||"Pekan" AA FROM fitrust1)C
